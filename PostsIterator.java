package sniterator;

public interface PostsIterator {
    boolean hasNext();
    Post getNext();
    void reset();
}
