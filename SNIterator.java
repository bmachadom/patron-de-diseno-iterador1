package sniterator;

public class SNIterator {
     public static void main(String[] args) {
        Twitter twitter = new Twitter();
        PostsIterator postsIterator = twitter.findPostsByHashtag("#Fundamento_diseno_software");
        while (postsIterator.hasNext()){
            Post post = postsIterator.getNext();
            System.out.println(post.getContent());
        }
    }
}
