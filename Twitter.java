package sniterator;

public class Twitter implements SocialNetwork {
    @Override
    public PostsIterator findPostsByHashtag(String hashtag) {
        return new TwitterIterator(hashtag);
    }
}