package sniterator;

public interface SocialNetwork {
    PostsIterator findPostsByHashtag(String hashtag);
}
