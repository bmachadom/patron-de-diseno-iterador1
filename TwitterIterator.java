package sniterator;

public class TwitterIterator implements PostsIterator {
    private String hashtag;
    private Post[] posts;
    private int position;

    public TwitterIterator(String hashtag) {
        this.hashtag = hashtag;
    }

    @Override
    public boolean hasNext() {
        loadPosts();
        return posts != null && posts.length > 0 && position < posts.length;
    }

    @Override
    public Post getNext() {
        return posts != null && position < posts.length ? posts[position++] : null;
    }

    @Override
    public void reset() {
        posts = null;
        position = 0;
    }

    private void loadPosts() {
        if (posts == null) {
            posts = new Post[]{
                    new Post(hashtag + " es util y necesario para el desarrollo de la carrera."),
                    new Post("El " + hashtag + " es vital para la creacion de buen software."),
                    new Post(hashtag)
            };
        }
    }
}
